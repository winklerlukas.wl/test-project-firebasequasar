import VueAnalytics from 'vue-analytics'

export default async ({ app, Vue }) => {
  Vue.use(VueAnalytics, {
    id: 'UA-143392291-1',
    router: app.router,
  })
};
